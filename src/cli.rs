use crate::InstallScript;
use owo_colors::OwoColorize;
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "argh", derive(argh::FromArgs))]
#[cfg_attr(feature = "argh", argh(subcommand, name = "install"))]
#[cfg_attr(feature = "clap", derive(clap::Args))]
/// self-install
pub struct Args {
    /// install into destdir instead of the root of the filesystem
    #[cfg_attr(feature = "argh", argh(option))]
    #[cfg_attr(feature = "clap", arg(long))]
    pub destdir: Option<PathBuf>,

    /// uninstall
    #[cfg_attr(feature = "argh", argh(switch))]
    #[cfg_attr(feature = "clap", arg(long))]
    pub uninstall: bool,

    /// don't install, only show the installation steps to run
    #[cfg_attr(feature = "argh", argh(switch))]
    #[cfg_attr(feature = "clap", arg(long))]
    pub details: bool,

    /// explicitly select profile
    #[cfg_attr(feature = "argh", argh(option, short = 'p'))]
    #[cfg_attr(feature = "clap", arg(long, short = 'p'))]
    pub profile: Option<String>,

    /// list available profiles
    #[cfg_attr(feature = "argh", argh(switch, short = 'l'))]
    #[cfg_attr(feature = "clap", arg(long, short = 'l'))]
    pub list_profiles: bool,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid profile name '{0}'")]
    InvalidProfile(String),
    #[error("error running installer")]
    InstallError(#[from] crate::InstallError),
    #[error("error running uninstaller")]
    UninstallError(#[from] crate::UninstallError),
}

#[derive(Debug)]
pub struct Installer {
    default: InstallScript,
    additional: Vec<InstallScript>,
}

impl Installer {
    pub fn new(default: InstallScript) -> Self {
        Installer {
            default,
            additional: vec![],
        }
    }

    pub fn add_additional(mut self, additional: InstallScript) -> Self {
        self.additional.push(additional);
        self
    }

    pub fn run(&mut self, args: Args) -> Result<(), Error> {
        if args.list_profiles {
            println!("{} ({})", self.default.name(), "default".blue());
            for script in self.additional.iter_mut() {
                println!("{}", script.name());
            }
            return Ok(());
        }

        let destination = args.destdir.into();
        let script = match &args.profile {
            Some(profile_name) if profile_name == self.default.name() => &mut self.default,
            Some(profile_name) => self
                .additional
                .iter_mut()
                .find(|s| s.name() == profile_name)
                .ok_or_else(|| Error::InvalidProfile(profile_name.clone()))?,
            None => &mut self.default,
        };

        if args.details {
            print!("{}", script.details());
        } else if args.uninstall {
            script.uninstall(&destination)?;
        } else {
            script.install(&destination)?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(feature = "argh")]
    mod test_argh {
        use super::*;

        #[test]
        fn argh_should_parse_args() {
            use argh::FromArgs;
            let result = Args::from_args(
                &["installer"],
                &[
                    "--destdir",
                    "/test-dir",
                    "--uninstall",
                    "--details",
                    "--profile",
                    "test-profile",
                    "--list-profiles",
                ],
            );

            assert_eq!(
                result.unwrap(),
                Args {
                    destdir: Some("/test-dir".into()),
                    uninstall: true,
                    details: true,
                    profile: Some("test-profile".into()),
                    list_profiles: true,
                }
            );
        }
    }

    #[cfg(feature = "clap")]
    mod test_clap {
        use super::*;

        #[derive(clap::Parser)]
        struct TestParser {
            #[command(flatten)]
            args: Args,
        }

        #[test]
        fn clap_should_parse_args() {
            use clap::Parser;
            let result = TestParser::parse_from([
                "installer",
                "--destdir",
                "/test-dir",
                "--uninstall",
                "--details",
                "--profile",
                "test-profile",
                "--list-profiles",
            ]);

            assert_eq!(
                result.args,
                Args {
                    destdir: Some("/test-dir".into()),
                    uninstall: true,
                    details: true,
                    profile: Some("test-profile".into()),
                    list_profiles: true,
                }
            );
        }
    }
}
