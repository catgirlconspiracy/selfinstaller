//! Installation steps that interact with dinit.

use crate::{Destination, Outcome};
use std::process::{Command, Stdio};

/// dinitctl mode
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Mode {
    System,
    User,
}

impl Mode {
    fn arg(&self) -> &str {
        match self {
            Mode::System => "--system",
            Mode::User => "--user",
        }
    }
}

/// Implementation struct for enabling a dinit service.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DinitEnable {
    mode: Mode,
    service: String,
}

impl crate::InstallStep for DinitEnable {
    fn install_description(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.mode {
            Mode::System => write!(f, "enable dinit service {}", self.service),
            Mode::User => write!(f, "enable user-session dinit service {}", self.service),
        }
    }

    fn uninstall_description(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.mode {
            Mode::System => write!(f, "disable dinit service {}", self.service),
            Mode::User => write!(f, "disable user-session dinit service {}", self.service),
        }
    }

    fn install(&self, destination: &Destination) -> eyre::Result<Outcome> {
        if destination.is_system() {
            dinitctl_enable(self.mode, &self.service)?;
            Ok(Outcome::Ok)
        } else {
            Ok(Outcome::Skipped("non-system destination".to_owned()))
        }
    }

    fn uninstall(&self, destination: &Destination) -> eyre::Result<Outcome> {
        if destination.is_system() {
            dinitctl_disable(self.mode, &self.service)?;
            Ok(Outcome::Ok)
        } else {
            Ok(Outcome::Skipped("non-system destination".to_owned()))
        }
    }
}

/// Implementation struct for starting a dinit service.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DinitStart {
    mode: Mode,
    service: String,
}

impl crate::InstallStep for DinitStart {
    fn install_description(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.mode {
            Mode::System => write!(f, "start dinit service {}", self.service),
            Mode::User => write!(f, "start user-session dinit service {}", self.service),
        }
    }

    fn uninstall_description(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.mode {
            Mode::System => write!(f, "stop dinit service {}", self.service),
            Mode::User => write!(f, "stop user-session dinit service {}", self.service),
        }
    }

    fn install(&self, destination: &Destination) -> eyre::Result<Outcome> {
        if destination.is_system() {
            dinitctl_start(self.mode, &self.service)?;
            Ok(Outcome::Ok)
        } else {
            Ok(Outcome::Skipped("non-system destination".to_owned()))
        }
    }

    fn uninstall(&self, destination: &Destination) -> eyre::Result<Outcome> {
        if destination.is_system() {
            dinitctl_stop(self.mode, &self.service)?;
            Ok(Outcome::Ok)
        } else {
            Ok(Outcome::Skipped("non-system destination".to_owned()))
        }
    }
}

fn dinitctl_enable(mode: Mode, service: &str) -> eyre::Result<()> {
    run_dinitctl(mode, Some("service already enabled"), ["enable", service])
}

fn dinitctl_disable(mode: Mode, service: &str) -> eyre::Result<()> {
    run_dinitctl(
        mode,
        Some("service not currently enabled"),
        ["disable", service],
    )
}

fn dinitctl_start(mode: Mode, service: &str) -> eyre::Result<()> {
    run_dinitctl(mode, None, ["start", service])
}

fn dinitctl_stop(mode: Mode, service: &str) -> eyre::Result<()> {
    run_dinitctl(mode, None, ["stop", service])
}

fn run_dinitctl<'a>(
    mode: Mode,
    unchanged_str: Option<&str>,
    args: impl IntoIterator<Item = &'a str>,
) -> eyre::Result<()> {
    let result = Command::new("dinitctl")
        .arg(mode.arg())
        .args(args)
        .stderr(Stdio::piped())
        .spawn()?
        .wait_with_output()?;
    let ok = match unchanged_str {
        Some(unchanged_str) if !result.status.success() => result
            .stderr
            .windows(unchanged_str.len())
            .any(|bs| bs == unchanged_str.as_bytes()),
        _ => result.status.success(),
    };

    if ok {
        Ok(())
    } else {
        Err(eyre::eyre!("{}", String::from_utf8_lossy(&result.stderr)))
    }
}

/// Create an installation step to enable a system-wide dinit service. If run
/// with a non-system destination, installation and uninstallation for this
/// step will do nothing.
///
/// ## Uninstallation
/// The service will be disabled.
///
/// ## Example
/// ```no_run
/// # use selfinstaller::{Destination, InstallStep, steps::dinit};
/// dinit::enable("networkmanager").install(&Destination::System)?;
/// # Ok::<(), eyre::Report>(())
pub fn enable(service: impl Into<String>) -> DinitEnable {
    let service = service.into();
    DinitEnable {
        mode: Mode::System,
        service,
    }
}

/// Create an installation step to enable a user-session dinit service. If run with a
/// non-system destination, installation and uninstallation for this step will do nothing.
///
/// ## Uninstallation
/// The user-session service will be disabled.
///
/// ## Example
/// ```no_run
/// # use selfinstaller::{Destination, InstallStep, steps::dinit};
/// dinit::enable_user("pipewire").install(&Destination::System)?;
/// # Ok::<(), eyre::Report>(())
pub fn enable_user(service: impl Into<String>) -> DinitEnable {
    let service = service.into();
    DinitEnable {
        mode: Mode::User,
        service,
    }
}

/// Create an installation step to start a system-wide dinit service. If run
/// with a non-system destination, installation and uninstallation for this
/// step will do nothing.
///
/// ## Uninstallation
/// The service will be stopped.
///
/// ## Example
/// ```no_run
/// # use selfinstaller::{Destination, InstallStep, steps::dinit};
/// dinit::start("networkmanager").install(&Destination::System)?;
/// # Ok::<(), eyre::Report>(())
pub fn start(service: impl Into<String>) -> DinitStart {
    let service = service.into();
    DinitStart {
        mode: Mode::System,
        service,
    }
}

/// Create an installation step to start a user-session dinit service. If run with a
/// non-system destination, installation and uninstallation for this step will do nothing.
///
/// ## Uninstallation
/// The user-session service will be stopped.
///
/// ## Example
/// ```no_run
/// # use selfinstaller::{Destination, InstallStep, steps::dinit};
/// dinit::start_user("pipewire").install(&Destination::System)?;
/// # Ok::<(), eyre::Report>(())
pub fn start_user(service: impl Into<String>) -> DinitStart {
    let service = service.into();
    DinitStart {
        mode: Mode::User,
        service: service,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::steps::testutil;
    use crate::InstallStep;
    use std::path::PathBuf;

    #[test]
    fn test_mode_arg_system() {
        assert_eq!(Mode::System.arg(), "--system");
    }

    #[test]
    fn test_mode_arg_user() {
        assert_eq!(Mode::User.arg(), "--user");
    }

    #[test]
    fn test_dinit_enable() {
        let step = enable("ssh.service");
        assert_eq!(step.mode, Mode::System);
    }

    #[test]
    fn test_dinit_enable_user() {
        let step = enable_user("bob.socket");
        assert_eq!(step.mode, Mode::User);
    }

    #[test]
    fn test_system_install_description() {
        let step = enable("srv.service");
        assert_eq!(
            &testutil::install_description(&step),
            "enable dinit service srv.service"
        );
    }

    #[test]
    fn test_user_install_description() {
        let step = enable_user("session.service");
        assert_eq!(
            &testutil::install_description(&step),
            "enable user-session dinit service session.service"
        );
    }

    #[test]
    fn test_system_uninstall_description() {
        let step = enable("srv.service");
        assert_eq!(
            &testutil::uninstall_description(&step),
            "disable dinit service srv.service"
        );
    }

    #[test]
    fn test_user_uninstall_description() {
        let step = enable_user("session.service");
        assert_eq!(
            &testutil::uninstall_description(&step),
            "disable user-session dinit service session.service"
        );
    }

    #[test]
    fn should_install_nothing_for_non_system_destination() {
        let step = enable("srv.service");

        let result = step.install(&Destination::DestDir(PathBuf::new())).unwrap();

        assert!(matches!(result, Outcome::Skipped(_)));
    }

    #[test]
    fn should_uninstall_nothing_for_non_system_destination() {
        let step = enable("srv.service");

        let result = step
            .uninstall(&Destination::DestDir(PathBuf::new()))
            .unwrap();

        assert!(matches!(result, Outcome::Skipped(_)));
    }

    #[test]
    fn test_dinit_start() {
        let step = start("ssh.service");
        assert_eq!(step.mode, Mode::System);
    }

    #[test]
    fn test_dinit_start_user() {
        let step = start_user("bob.socket");
        assert_eq!(step.mode, Mode::User);
    }

    #[test]
    fn should_start_nothing_for_non_system_destination() {
        let step = start("srv.service");

        let result = step.install(&Destination::DestDir(PathBuf::new())).unwrap();

        assert!(matches!(result, Outcome::Skipped(_)));
    }

    #[test]
    fn should_stop_nothing_for_non_system_destination() {
        let step = start("srv.service");

        let result = step
            .uninstall(&Destination::DestDir(PathBuf::new()))
            .unwrap();

        assert!(matches!(result, Outcome::Skipped(_)));
    }
}
