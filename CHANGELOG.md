# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## UNRELEASED

* Rename `SelfInstaller` type to `InstallScript`.
* Add an obligatory name parameter to every `InstallScript`.
* Fix systemd "start" step.
* Add a standard set of command-line arguments.
* Add dinit steps.

## 0.2.0

Initial release.
